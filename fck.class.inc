<?php
/**
 * @file
 * Contains field handler of fck. And some other utility stuff to deal with
 * defined field types.
 */

// _________________________________________________ HELPERS.
/**
 * Helper function containing all required queries for this module.
 * 
 * Since handlers are context dependent, A context is always required. For now
 * it can be any of field, widget or formatter.
 *
 * @TODO validaet query name at first.
 *
 * @param string $query_name
 *   All queries have a name, List of them can be seen in switch statement.
 * @param string $context
 *   All queries here only need one condition to run (If any), And it should be
 *   given as third argument.
 * @param mixed $condition0
 *   Some queries but not all of them need a condition, If so, this is 
 *   the condition!
 */
function _fck_query($query_name, $context, $condition0 = NULL, $condition1 = NULL) {
  // Context is always required.
  if (!isset($context) || !$context) {
    throw new Exception('FCK Query: Context is not given');
  }

  // Information of all available handlers in a context.
  if ($query_name == 'all info') {
    return db_select('fck_handler', 'fh')
      ->condition('fh.context', $context, '=')
      ->fields('fh')
      ->execute()->fetchAll();
  }

  // Uid of all available handlers in a context.
  if ($query_name == 'handler uids') {
    return db_select('fck_handler', 'fh')
      ->condition('fh.context', $context, '=')
      ->fields('fh', array('uid'))
      ->execute()->fetchCol();
  }

  // Other queries below require a condition.
  if (!isset($condition0) || !$condition0) {
    throw new Exception('FCK Query: Condition is not given');
  }

  // Check to see if an FCK handler for given field type exists.
  if ($query_name == 'handler exists') {
    return db_select('fck_handler', 'fh')
      ->condition('fh.context', $context, '=')
      ->condition('fh.uid', $condition0, '=')
      ->fields('fh', array('hid'))
      ->range(0,1)
      ->execute()
      ->rowCount();
  }

  // Retrive mapping of two handlers to each other. This one does not fit with 
  // others! 
  // $context is actually $condition0, as source handler UID, and $condition0 
  // should be $condition1, as target handler UID.
  if ($query_name == 'mapping') {
    return db_select('fck_map', 'fm')
      ->condition('fm.owner', $context)
      ->condition('fm.target', $condition0)
      ->fields('fm', array('owner_sub_uid', 'target_sub_uid'))
      ->execute()->fetchAll();
  } 

  // Available subhandlers in a handler given it's uid in a context.
  if ($query_name == 'subhandlers') {
    static $subhandlers_query 
      = 'fh.hid = fs.parent AND fh.context = :ctx AND fh.uid = :uid';
    $args = array(':ctx' => $context, ':uid' => $condition0);
    $result = db_select('fck_handler', 'fh');
    $result->join('fck_subhandler', 'fs', $subhandlers_query, $args);
    return $result->fields('fs')->execute()->fetchAll();
  }

  // Info of a handler given it's uid.
  if ($query_name == 'info') {
    return db_select('fck_handler', 'fh')
      ->condition('fh.context', $context, '=')
      ->condition('fh.uid', $condition0, '=')
      ->fields('fh')
      ->execute()->fetchAssoc();
  }

  // Other queries below require a second condition.
  if (!isset($condition1) || !$condition1) {
    throw new Exception('FCK Query: Condition is not given');
  }

  // Available subhandlers in a handler given it's uid in a context and their
  // mapping info against another handler.
  if ($query_name == 'mapped subhandlers') {
    static $m_subhandlers_query0 
      = 'fh.hid = fs.parent AND fh.context = :ctx AND fh.uid = :uid';
    $args0 = array(':ctx' => $context, ':uid' => $condition0);

    static $m_subhandlers_query1 
      = 'fm.owner = fh.uid AND fm.owner_sub_uid = fs.uid AND fm.target = :target';
    $args1 = array(':target' => $condition1);

    $result = db_select('fck_handler', 'fh');
    $result->join('fck_subhandler', 'fs', $m_subhandlers_query0, $args0);
    $result->join('fck_map', 'fm', $m_subhandlers_query1, $args1);
    $result->fields('fs')->fields('fm', array('target_sub_uid'));
    return $result->execute()->fetchAll();
  }

  // By now we are sure query is invalid.
  throw new Exception('FCK Query: Invalid query: ' . $query_name);
}

/**
 * Helper to return an storage containing instance of all available handlers.
 */
function _fck_all_handlers($context) {
  $handlers = new SplObjectStorage();

  foreach (_fck_query('handler uids', $context) as $qresult) {
    $handlers->attach(_fck($context, $qresult));
  }

  return $handlers;
}

/**
 * Heler function to get class name of a plugin.
 *
 * Subhandler are implemented through ctools plugin interface and this helper
 * is used to get name of it's class to instanciate.
 *
 * If type of requested plugin is not given, Then all of themed are fetched,
 * But this function makes sure the returned class actually exists. If type
 * is given, NULL is returned of that single requested class does not exist.
 */
function _fck_get_plugin_class($context, $type = NULL) {
  ctools_include('plugins');
  $plugins = ctools_get_plugins('fck', $context, $type);

  if ($type) {
    $handlers = ctools_plugin_get_class($plugins, 'handler');
  }
  else {
    $handlers = array();
    foreach ($plugins as $plugin) {
      $handler = ctools_plugin_get_class($plugin, 'handler');
      if (class_exists($handler)) {
        $handlers[] = $handler;
      }
    }
  }

  return $handlers;
}

/**
 * Finds handler which have same number of different subhandlers.
 *
 * The matching handlers should have same number of each subhandler type. This
 * is useful for finding which fields a widget supports.
 *
 * @param FCKContainer $source
 *   An instance of a handler.
 * @param string $target
 *   Context which target handlers should belong to. For instance if source
 *   handler is a widget, Target handler could be field.
 */
function _fck_matching_handlers(FCKContainer $source, $target) {
  // Counts number of each subhandler type a handler has.
  $target_subcount = array();
  $all_handlers = _fck_all_handlers($target); 
  foreach ($all_handlers as $handler) {
    $target_subcount[$handler->uid()] = array_count_values($handler->getProperty('type'));
  }

  // Number of each sunhandler type source handler contains.
  $source_subcount = array_count_values($source->getProperty('type'));

  $matches = array();
  foreach ($target_subcount as $uid => $count) {
    if ($source_subcount == $count) {
      $matches[] = $uid;
    }
  }

  return $matches;
}

// _________________________________________________ CLASSES.
/**
 * Base class for plugins (subhandlers) to extend. 
 *
 * Contains some utility function only, And it isnt the best OO design!
 */
abstract class FCKBase {
  /**
   * Information of subhandler such as uid and other stuff as an array.
   */
  protected $info;

  /**
   * Default constructor to gather information of this subhandler instance.
   */
  public function __construct($info) {
    $this->info = $info;
  }

  /**
   * Returuns requested info of this subhandler or all of theme.
   */
  public function info($name = NULL) {
    if ($name !== NULL) {
      if(isset($this->info->{$name})) {
        return $this->info->{$name};
      }
    }
    else {
      return $this->info;
    }
  }

  /**
   * Helper method to find values related to this subhandler in an array.
   *
   * Given values in array sent to subhandler contain values of other 
   * subhandlers too, They are prefixed with subhandlers uid.
   */
  public function getValues($values, $names) {
    $result = array();
    static $x=1;
    $x&&ddebug_backtrace();
    $x&&dpm($values);
    $x&&dpm($names);
    $x=0;
    foreach ($names as $name) {
      $result[] = $values[$this->info->uid . '_' . $name];
    }

    return $result;
  }

  /**
   * Prefixed array keys of $values with $prefix.
   *
   * If $add_uid is TRUE, Final array key will be lik $prefix . $uid . $key.
   */
  public function prefix($values, $prefix, $add_uid=TRUE) {
  }

  /**
   * Returns UID of mapped subhandler to this subhandler.
   */
  public function target() {
    return $this->info('target_sub_uid');
  }
}

/**
 * Main handler class which will hold subhandlers and gather data from them.
 */
class FCKContainer extends SplObjectStorage {
  /**
   * Information of subhandler such as uid and other stuff as an array.
   */
  protected $info;

  /**
   * Cached data from subhandlers results.
   */
  protected $result = array();

  /**
   * Just stores a handlers info.
   */
  public function __construct($info) {
    $this->info = $info;
  }

  /**
   * Returns Info of subhandler.
   */
  public function info($name = NULL) {
    if ($name !== NULL) {
      if(isset($this->info[$name])) {
        return $this->info[$name];
      }
    }
    else {
      return $this->info;
    }
  }

  /**
   * Returns uid of this handler given in info.
   */
  public function uid() {
    return $this->info['uid'];
  }

  // __________________________________________________________________________
  /**
   * Gatheres properties of subhandlers.
   *
   * @param string $property
   *   Name of property to fetch.
   * @param string $set_name
   *   Name to prefix result of subhandler calls to prefix with. If it is
   *   evaluated to FALSE, It is ignored and nothing is prefixed.
   * @param bool $reset
   *   Reset cached data (if TRUE).
   */
  public function getProperty($property, $set_name = '', $empty_behaviour = FALSE, $reset = FALSE) {
    // Make sure cacahed data is valid.
    if (!isset($this->result[$set_name][$property]) || $reset || $this->count() !== count($this->result[$set_name][$property])) {
      // Reset cached data.
      $this->result[$set_name][$property] = array();

      foreach ($this as $subhandler) {
        $result = $subhandler->info($property);

        if (!empty($result)) {
          if(is_array($result) && $set_name) {
            $result = $this->setName($result, $subhandler->info('uid'));
            $this->result[$set_name][$property] += $result;
          }
          else {
            $this->result[$set_name][$property][] = $result;
          }
        }
      }
    }

    return $this->result[$set_name][$property];
  }

  /**
   * Gatheres processed data by subhandlers.
   *
   * Makes sure method exists on each subhandler and if not, Ignores the 
   * subhandler which do not implement that method.
   *
   * @param string $method
   *   Subhandler's method responsible for processing data.
   * @param array $args
   *   An array of arguments send to each subhandler.
   * @param mixed $arg1
   *   A referenced argument which subhandlers can change (one of two).
   * @param mixed $arg2
   *   A referenced argument which subhandlers can change (two of two).
   */
  public function get($method, array $args = array(), &$arg1 = NULL, &$arg2 = NULL) {
    $this->result[$method] = array();

    foreach ($this as $subhandler) {
      if (method_exists($subhandler, $method)) {
        if ($result = $subhandler->{$method}($this, $args, $arg1, $arg2)) {
          $this->result[$method] += $this->setName($result, $subhandler->info('uid'));
        }
      }
    }

    return $this->result[$method];
  }

  /**
   * Gatheres processed data by subhandlers.
   *
   * Makes sure method exists on each subhandler and if not, Ignores the 
   * subhandler which do not implement that method.
   *
   * It's difference from get() is that it won't try to prefix returned values 
   * with subhandler uid, And it should be done by subhandler itself.
   *
   * @param string $method
   *   Subhandler's method responsible for processing data.
   * @param array $args
   *   An array of arguments send to each subhandler.
   * @param mixed $arg1
   *   A referenced argument which subhandlers can change (one of two).
   * @param mixed $arg2
   *   A referenced argument which subhandlers can change (two of two).
   */
  public function getRaw($method, array $args = array(), &$arg1 = NULL, &$arg2 = NULL) {
    $this->result[$method] = array();

    foreach ($this as $subhandler) {
      if (method_exists($subhandler, $method)) {
        if ($result = $subhandler->{$method}($this, $args, $arg1, $arg2)) {
          $this->result[$method] += $result;
        }
      }
    }

    return $this->result[$method];
  }

  /**
   * Send args to each subhandler for processing calling their method.
   *
   * If method is not implemented by subhandler, They are ignored. Return data
   * should be valid as a boolean.
   *
   * @param string $method
   *   Subhandler's method responsible for processing data.
   * @param array $args
   *   An array of arguments send to each subhandler.
   * @param bool $only
   *   A boolean indicating whether all returned values should be $only=TRUE or
   *   $only=FALSE.
   */
  public function getBoolean($method, array $args = array(), $only = TRUE) {
    $this->result[$method] = array();

    foreach ($this as $subhandler) {
      if (method_exists($subhandler, $method)) {
        $this->result[$method][] = $subhandler->{$method}($this, $args);
      }
    }

    return !in_array(!$only, $this->result[$method]);
  }

  /**
   * Gives some data to subhandlers to process. Returned data are ignored.
   *
   * Pretty much same as get() just that returned data are ignored.
   *
   * @param string $method
   *   Subhandler's method responsible for processing data.
   * @param mixed $args
   *   An array of arguments send to each subhandler.
   * @param mixed $arg1
   *   A referenced argument which subhandlers can change (one of two).
   * @param mixed $arg2
   *   A referenced argument which subhandlers can change (two of two).
   */
  public function call($method, $args = NULL, &$arg1 = NULL, &$arg2 = NULL) {
    foreach ($this as $subhandler) {
      if (method_exists($subhandler, $method)) {
        $subhandler->{$method}($this, $args, $arg1, $arg2);
      }
    }
  }


  // __________________________________________________________________________
  /**
   * Not implemented yet.
   */
  public function __sleep() {
    $this->notImplemented(__FUNCTION__);
  }

  /**
   * Not implemented yet.
   */
  public function __wakeup() {
    $this->notImplemented(__FUNCTION__);
  }

  /**
   * Not implemented yet.
   */
  public function __clone() {
    $this->notImplemented(__FUNCTION__);
  }

  /**
   * Helper method which will be removed later.
   */
  public function notImplemented($name) {
    throw new BadMethodCallException($name . ' is not implemented yet.');
  }

  /**
   * Prefix all keys of $value array with $name and recurse to $values.
   *
   * @param array $value
   *   Array of values to process.
   * @param string $name
   *   String which array keys should be prefixed with.
   */
  public static function setNameRecursive(array $value, $name) {
    $new = array();

    foreach ($values as $key => $data) {
      if (is_array($data)) {
        $data = self::setNameRecursive($data, $name);
      }
      $new[$name . '_' . $key] = $data;
    }

    return $new;
  }

  /**
   * Recurse to $values and change it's keys. 
   *
   * A $length of charachter is removed from keys.
   */
  public static function unsetRecursive(array $values, $length) {
    $new = array();

    foreach ($values as $key => $data) {
      if (is_array($data)) {
        $data = self::unsetRecursive($data, $length);
      }
      $new[substr($key, $length)] = $data;
    }

    return $new;
  }

  /**
   * De-prefix array keys from $name.
   */
  public static function unsetNameRecursive(array $values, $name) {
    return self::unsetRecursive($values, strlen($name) + 1);
  }

  /**
   * Prefix keys of an array with $name.
   */
  public static function setName(array $values, $name) {
    $new = array();
    foreach ($values as $key => $data) {
      $new[$name . '_' . $key] = $data;
    }

    return $new;
  }

  /**
   * Unprefix keys of array only if they are prefixed with $name.
   *
   * @param array $values
   *   Array to process.
   * @param string $name
   *   Name to check against if array keys are prefixed with.
   * @param int $length
   *   Length of $name.
   */
  public static function checkUnsetRecursive(array $values, $name, $length) {
    $new = array();

    foreach ($values as $key => $data) {
      if (is_array($data)) {
        $data = self::unsetNameRecursive($data, $length);
      }
      if (substr($key, 0, $length) === $name) {
        $new[substr($key, $length)] = $data;
      }
    }

    return $new;
  }
}

/**
 * Factory function for FCKContainer.
 *
 * @param string $context
 *   Context which this handler belongs to.
 * @param string $uid
 *   Uid of handler to fetch from database.
 * @param string $target
 *   If $context is widet or field, Target UID of field for which a widget or 
 *   formatter is being created, Is needed.
 */
function _fck($context, $uid, $target = NULL) {
  $info = _fck_query('info', $context, $uid);

  // Extra is serialized in database, Unserialize it now.
  $info['extra'] = unserialize($info['extra']);

  $handler = new FCKContainer($info);

  // Widget and formatter need mapping against another handler (field handler).
  $query = ($target) ? 'mapped subhandlers' : 'subhandlers';

  // Attach coresponding sunhandlers.
  foreach (_fck_query($query, $context, $uid, $target) as $qresult) {
    $plugin = _fck_get_plugin_class($context, $qresult->type);
    $handler->attach(new $plugin($qresult));
  }

  return $handler;
}

