<?php

/**
 * @file
 * Primary Drupal Field API hooks.
 * 
 * This is where everyting starts, Whenever a hook is called, A handler of
 * requested field type is inastanced using _fck(), Arguments of the called
 * hook are directly passed to handler and result is returned (if any).
 * 
 * To see how a handler works see comment for class FCKContainer.
 */

function _fck_iamlazy($function_name, $args) {
  static $cache;
  if (!$cache) {
    $cache = drupal_static(__FUNCTION__, array());
  }

  if (!isset($cache[$function_name]['reflector'])) {
    $cache[$function_name] = array();
    $reflector = new ReflectionFunction($function_name);
    foreach ($reflector->getParameters() as $param) {
      $cache[$function_name][] = $param->name;
    }
  }

  $keyed_args = array();
  foreach ($cache[$function_name] as $index => $name) {
    $keyed_args[$name] = $args[$index];
  }

  return $keyed_args;
}

/**
 * I've no idea what this is, But looks like a good thing to implement.
 */
function _fck_property_info_callback(&$info, $entity_type, $field, $instance, $field_type) {
}

// INFO__________________________________________
/**
 * Implements hook_field_info().
 */
function fck_field_info() {
  $fields = array();
  foreach (_fck_all_handlers('field') as $handler) {
    $info = $handler->info();
    $fields[$handler->uid()] = array(
      'label' => $info['label'],
      'description' => isset($info['extra']['description']) ? $info['extra']['description'] : t('FCK Field'),
      'no_ui' => isset($info['extra']['no_ui']) ? $info['extra']['no_ui'] : FALSE ,
      'default_widget' => 'fck',
      'default_formatter' => 'fck',
      'settings' => $handler->getProperty('settings', TRUE),
      'instance_settings' => $handler->getProperty('instanceSettings', TRUE),
    );
  }

  return $fields;
}

/**
 * Implements hook_field_formatter_info().
 */
function fck_field_formatter_info() {
  $formatter['fck'] = array(
    'label' => t('FCK default formatter'),
    'descrition' => t('FCK default formatter has no implemention yet.'),
    'field types' => _fck_query('handler uids', 'formatter'),
  );

  foreach (_fck_all_handlers('formatter') as $handler) {
    $label = $handler->info('label');
    $uid = $handler->info('uid');

    $formatter['fck_' . $uid] = array(
      'label' => 'FCK ' . isset($label) ? $label : $uid,
      'description' => t('Created through Field Construction Kit'),
      'field types' => _fck_matching_handlers($handler, 'field'),
      'settings' => $handler->getProperty('settings', TRUE),
    );

    $formatter['fck_' . $uid]['fck_display_column_label'] = TRUE;
  }

  return $formatter;
}

/**
 * Implements hook_field_widget_info().
 */
function fck_field_widget_info() {
  return _fck_widget_formatter_helper('widget');
}

/**
 * Helper function to gather widget/formatter info from handlers.
 *
 * Called by hook_field_widget_info and hook_field_formatter_info of
 * this module only.
 */
function _fck_widget_formatter_helper($contex) {
  $result['fck'] = array(
    'label' => ($contex == 'widget') ? t('FCK default widget') : t('FCK default formatter'),
    'descrition' => t('FCK default formatter/widget has no implemention yet.'),
    'field types' => array(),
  );

  foreach (_fck_all_handlers('field') as $handler) {
    $result['fck']['field types'][] = $handler->info('uid');
  }

  foreach (_fck_all_handlers($contex) as $handler) {
    $label = $handler->info('label');
    $uid = $handler->info('uid');

    $result['fck_' . $uid] = array(
      'label' => 'FCK ' . isset($label) ? $label : $uid,
      'description' => t('Created through Field Construction Kit'),
      'field types' => _fck_matching_handlers($handler, 'field'),
      'settings' => $handler->getProperty('settings', TRUE),
    );
  }

  return $result;
}

// FIELD ________________________________________
/**
 * Implements hook_field_delete().
 */
function fck_field_delete($entity_type, $entity, $field, $instance, $langcode, &$items) {
  $args = _fck_iamlazy(__FUNCTION__, func_get_args());
  _fck('field', $field['type'])->call('delete', $args, $items);
}

/**
 * Implements hook_field_delete_revision().
 */
function fck_field_delete_revision($entity_type, $entity, $field, $instance, $langcode, &$items) {
  $args = _fck_iamlazy(__FUNCTION__, func_get_args());
  return _fck('field', $field['type'])->call('deleteRevision', $args, $items);
}

/**
 * Implements hook_field_insert().
 */
function fck_field_insert($entity_type, $entity, $field, $instance, $langcode, &$items) {
  $args = _fck_iamlazy(__FUNCTION__, func_get_args());
  _fck('field', $field['type'])->call('insert', $args, $items);
}

/**
 * Implements hook_field_is_empty().
 */
function fck_field_is_empty($item, $field) {
  $args = _fck_iamlazy(__FUNCTION__, func_get_args());
  return _fck('field', $field['type'])->getBoolean('isEmpty', $args);
}

/**
 * Implements hook_field_load().
 */
function fck_field_load($entity_type, $entities, $field, $instances, $langcode, &$items, $age) {
  $args = _fck_iamlazy(__FUNCTION__, func_get_args());
  _fck('field', $field['type'])->call('load', $args, $items);
}

/**
 * Implements hook_field_prepare_translation().
 */
function fck_field_prepare_translation($entity_type, $entity, $field, $instance, $langcode, &$items, $source_entity, $source_langcode) {
  $args = _fck_iamlazy(__FUNCTION__, func_get_args());
  _fck('field', $field['type'])->call('prepareTranslation', $args, $items);
}

/**
 * Implements hook_field_prepare_view().
 */
function fck_field_prepare_view($entity_type, $entities, $field, $instances, $langcode, &$items) {
  $args = _fck_iamlazy(__FUNCTION__, func_get_args());
  _fck('field', $field['type'])->call('prepareView', $args, $items);
}

/**
 * Implements hook_field_presave().
 */
function fck_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  $args = _fck_iamlazy(__FUNCTION__, func_get_args());
  _fck('field', $field['type'])->call('presave', $args, $items);
}

/**
 * Implements hook_field_update().
 */
function fck_field_update($entity_type, $entity, $field, $instance, $langcode, &$items) {
  $args = _fck_iamlazy(__FUNCTION__, func_get_args());
  _fck('field', $field['type'])->call('update', $args, $items);
}

/**
 * Implements hook_field_validate().
 */
function fck_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  $args = _fck_iamlazy(__FUNCTION__, func_get_args());
  _fck('field', $field['type'])->call('validate', $args, $errors);
}

// __________________________________________
/**
 * Implements hook_field_widget_form().
 */
function fck_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $args = _fck_iamlazy(__FUNCTION__, func_get_args());
  $uid = substr($instance['widget']['type'], 4);
  $widget = _fck('widget', $uid, $field['type'])
    ->getRaw('widgetForm', $args, $form, $form_state);

  $widget += array(
    '#element_validate' => array('fck_widget_validate'),
    '#fck_data' => $args,
 );

  return $widget;
}

/**
 * General validation function for fck widgets.
 */
function fck_widget_validate($element, &$form_state, $form) {
  // #fck_data is arguments passed to fck_field_widget function, They are
  // stored in #fck_data to make it easier to find field and widget type,
  // And also maybe some column will benefit from this data.

  // Widget name will be like fck_WIDGET_NAME, So fist 4 charachters should be
  // removed to get actual widget name.
  $widget = substr($element['#fck_data'][3]['widget']['type'], 4);
  $field = $element['#fck_data'][2]['type'];

  $args = _fck_iamlazy(__FUNCTION__, func_get_args());
  $widget = _fck('widget', $widget, $field)
    ->call('widgetValidate', $args, $form_state);
}

/**
 * Implements hook_field_widget_error().
 */
function fck_field_widget_error($element, $error, $form, &$form_state) {
  $args = _fck_iamlazy(__FUNCTION__, func_get_args());
  _fck('field', $field['type'])->get('widgetError', $args, $form, $form_state);
}

// __________________________________________
/**
 * Implements hook_field_formatter_prepare_view().
 */
function fck_field_formatter_prepare_view($entity_type, $entities, $field, $instances, $langcode, &$items, $displays) {
  $args = _fck_iamlazy(__FUNCTION__, func_get_args());
  _fck('field', $field['type'])->get('formatterPrepareView', $args, $items);
}

/**
 * Implements hook_field_formatter_view().
 */
function fck_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $args = _fck_iamlazy(__FUNCTION__, func_get_args());
  $result = '';
  $uid = substr($display['type'], 4);
  $handler = _fck('formatter', $uid, $field['type']);

  $element = array();
  foreach ($items as $delta => $item) {
    $args[7] = $delta;
    $element[0] = $handler->getRaw('formatterView', $args, $element);
  }

  return $element;
}

// __________________________________________
/**
 * Implements hook_field_instance_settings_form().
 */
function fck_field_instance_settings_form($field, $instance) {
  $args = _fck_iamlazy(__FUNCTION__, func_get_args());
  return _fck('field', $field['type'])->get('instanceSettingsForm', $args);
}

function fck_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $args = _fck_iamlazy(__FUNCTION__, func_get_args());
  $formatter = substr($instance['display'][$view_mode]['type'], 4);
  $element = _fck('formatter', $formatter, $field['type'])
    ->get('formatterSettingsForm', $args, $form_state);

  $settings = $instance['display'][$view_mode]['settings'];
  $element['fck_display_column_label'] = array(
    '#type' => 'select',
    '#options' => array(
      'table' => t('Use table formatter'),
      'html' => t('Use plain html formatter'),
    ),
  );

  return $element;
}

function fck_field_formatter_settings_summary($field, $instance, $view_mode) {
  return t('FCK display formatter.');
}
