<?php

$plugin = array(
  'label' => t('FCK Number Integer'),
  'handler' => array('class' => 'FCKNumberIntegerField'),
);

class FCKNumberIntegerField extends FCKBase {
  public function __construct($info) {
    parent::__construct($info);

    $this->type = 'number_integer';
    $this->info->instanceSettings = array(
      'min' => '',
      'max' => '',
      'prefix' => '',
      'suffix' => ''
    );
  }

  public function uid() {
    return $this->info->uid;
  }

  public function columns() {
    $schema[$this->info->uid] = array(
      'type' => 'int',
      'not null' => FALSE,
    );

    return $schema;
  }

  public function validate($parent, $args, &$errors) {
    $name = array('min', 'max'); 

    list($min, $max) = $this->getValues($args['instance']['settings'], $name);
    $name = $this->info->uid;
    $label = isset($this->info->label) ? $this->info->label : $this->info->uid;

    foreach ($args['items'] as $delta => $item) {
      if ($item[$name] != '') {
        if (is_numeric($min) && $item[$name] < $min) {
          $error[$args['field']['field_name']][$arg['langcode']][$delta][] = array(
            'error' => 'number_min',
            'message' => t('%name: the value may be no less than %min.', array('%name' => $label, '%min' => $min)),
          );
        }
        elseif (is_numeric($max) && $item[$name] > $max) {
          $error[$args['field']['field_name']][$arg['langcode']][$delta][] = array(
            'error' => 'number_max',
            'message' => t('%name: the value may be no greater than %max.', array('%name' => $label, '%max' => $max)),
          );
        }
      }
    }
  }

  public function isEmpty($parent, $args) {
    $name = $this->info->uid;
    return (empty($args['item'][$name]) && ((string) $args['item'][$name] !== '0'));
  }

  // ___________________________________ FIELD UI
  public function instanceSettingsForm($parent, $args) {
    $uid = $this->info->uid;
    $name = array('@name' => $this->info->label . ' (' . $uid . ')');

    $form['min'] = array(
      '#type' => 'textfield',
      '#title' => t('@name Minimum', $name),
      '#default_value' => $args['instance']['settings'][$uid . '_min'],
      '#description' => t('The minimum value that should be allowed in this column. Leave blank for no minimum.'),
      '#element_validate' => array('element_validate_number'),
    );
    $form['max'] = array(
      '#type' => 'textfield',
      '#title' => t('@name Maximum', $name),
      '#default_value' => $args['instance']['settings'][$uid . '_max'],
      '#description' => t('The maximum value that should be allowed in this column. Leave blank for no maximum.'),
      '#element_validate' => array('element_validate_number'),
    );
    $form['prefix'] = array(
      '#type' => 'textfield',
      '#title' => t('@name Prefix', $name),
      '#default_value' => $args['inastance']['settings'][$uid . '_prefix'],
      '#size' => 60,
      '#description' => t("Define a string that should be prefixed to the value, like '$ ' or '&euro; '. Leave blank for none. Separate singular and plural values with a pipe ('pound|pounds')."),
    );
    $form['suffix'] = array(
      '#type' => 'textfield',
      '#title' => t('@name Suffix', $name),
      '#default_value' => $args['instance']['settings'][$uid . '_suffix'],
      '#size' => 60,
      '#description' => t("Define a string that should be suffixed to the value, like ' m', ' kb/s'. Leave blank for none. Separate singular and plural values with a pipe ('pound|pounds')."),
    );

    return $form;
  }
}

