<?php

$plugin = array(
  'label' => t('FCK Number Integer'),
  'handler' => array('class' => 'FCKNumberIntegerFormatter'),
);

class FCKNumberIntegerFormatter extends FCKBase {
  public static $settings = array(
    'thousand_separator' => '',
    'prefix_suffix' => '',
  );

  public function __construct($info) {
    parent::__construct($info);

    $this->type = 'number_integer';
    $this->info->settings = array(
      'thousand_separator' => '',
      'prefix_suffix' => TRUE,
    );
  }

  public function formatterView($parent, $args, &$element) {
//    $settings = $args[6]['settings'];
//    $name = array('thousand_separator', 'prefix_suffix');
//   list($thousand_sepirator, $prefix_suffix) = $this->getValues($settings, $name);

//    $settings = $args[3]['settings'];
//    $name = array('prefix', 'suffix');
      //    list($prefix, $suffix) = $this->getValues($settings, $name);

      $delta = $args['delta'];
      $map = $this->info('target_sub_uid');
      $output = number_format($args['items'][$delta][$map], 0, '', '');//, $thousand_sepirator);
      $elements[$this->info('uid')] = array('#markup' => $output);
dpm($elements);
    return $elements;
  }

  public function formatterSettingsForm($parent, $args, $form_state) {
    $name = array('thousand_separator', 'prefix_suffix');
    $settings = $args['instance']['display'][$args['view_mode']]['settings'];
    list($thousand_sepirator, $prefix_suffix) = $this->getValues($settings, $name);

    $options = array(
      ''  => t('<none>'),
      '.' => t('Decimal point'),
      ',' => t('Comma'),
      ' ' => t('Space'),
    );

    $element['thousand_separator'] = array(
      '#type' => 'select',
      '#title' => t('Thousand marker'),
      '#options' => $options,
      '#default_value' => $thousand_sepirator,
    );

    $element['prefix_suffix'] = array(
      '#type' => 'checkbox',
      '#title' => t('Display prefix and suffix.'),
      '#default_value' => $prefix_suffix,
    );

    return $element;
  }

  public function formatterSettingsSummary($parent, $args) {
    $settings = $args['instance']['display'][$args['view_mode']]['settings'];
    $name = array('thousand_separator', 'prefix_suffix');
    list($thousand_sepirator, $prefix_suffix) = $this->getValues($settings, $name);

    $summary[] = number_format(1234.1234567890, 0, '', $thousand_sepirator);
    if ($prefix_suffix) {
      $summary[] = t('Display with prefix and suffix.');
    }

    return implode('<br />', $summary);
  }

  public function settings() {
    return $this->settings;
  }
}
