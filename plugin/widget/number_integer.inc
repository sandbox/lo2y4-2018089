<?php 

$plugin = array(
  'label' => t('FCK Number Integer'),
  'handler' => array('class' => 'FCKNumberIntegerWidget'),
);

class FCKNumberIntegerWidget extends FCKBase {
  /**
   * @TODO prefix suffix support.
   */
  public function widgetForm($parent, $args, $form, $form_state) {
    if (isset($args['items'][$args['delta']])) {
      $value = $args['items'][$args['delta']];
      list($value) = $this->getValues($value, array('value'));
    }
    else {
      $value = '';
    }

    $element[$this->target()] = array(
      '#type' => 'textfield',
      '#default_value' => $value,
      '#size' => 12,
      '#maxlength' => 10,
      '#number_type' => 'number_integer',
    );

    return $element;
  }

  public function widgetError($parent, $args, $form, $form_state) {

  }
 
  public function widgetValidate($parent, $args, &$form_state) {
    $element = $args['element'][$this->info('target_sub_uid')];
    if ($element['#value'] != preg_replace('@([^-0-9])|(.-)@', '', $element['#value'])) {
      $message = t('Only numbers are allowed in %field.', array('%field' => 'COLUMN LABEL'));
      form_error($element, $message);      
    }
    else {
      // @TODO Core does not do this, Why am I doing this then?
      // Without this, Database layer gives fatal error, as stated in documents
      // '' is not NULL, But i thought that was schema defination only?
      if ($element['#value'] === '') {
        $element['#value'] = NULL;
      }

      form_set_value($element, $element['#value'], $form_state);
    }
  }
}
